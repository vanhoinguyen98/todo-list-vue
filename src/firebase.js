import firebase from 'firebase'
import 'firebase/firestore'

const config = {
  apiKey: 'AIzaSyC8FqXInP-PWnMNHhbi6qX23lm-v6FD01M',
  authDomain: 'authennodejs.firebaseapp.com',
  databaseURL: 'https://authennodejs.firebaseio.com',
  projectId: 'authennodejs',
  storageBucket: 'authennodejs.appspot.com',
  messagingSenderId: '1020779761335'
}

const firebaseApp = firebase.initializeApp(config)

const firestore = firebaseApp.firestore()

export default firestore
