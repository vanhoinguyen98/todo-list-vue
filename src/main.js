import Vue from 'vue'
import App from './App.vue'
import * as firebase from 'firebase'
import './registerServiceWorker'
import routes from './router'
import store from './store'
import VueRouter from 'vue-router'

window.eventBus = new Vue()

Vue.config.productionTip = false

Vue.use(VueRouter)

const router = new VueRouter({
  routes,
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.loggedIn) {
      next({
        name: 'login',
      },
        alert("Bạn chưa đăng nhập nè !")
      )
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresVisitor)) {
    if (store.getters.loggedIn) {
      next({
        name: 'home',
      },
        alert("Bạn đã đăng nhập rồi nè !")
      )
    } else {
      next()
    }
  } else {
    next()
  }
})

new Vue({
  router: router,
  store: store,
  render: h => h(App)
}).$mount('#app')
